<?php
get_header(); ?>

    <div id="home">
        <?php get_template_part('template/sessao', 'home') ?>
    </div>

    <div id="galeria">
        <?php get_template_part('template/sessao', 'galeria') ?>
    </div>

    <div id="lojistas">
        <?php get_template_part('template/sessao', 'lojistas') ?>
    </div>

     <div id="conte-com-a-adhara">
        <?php get_template_part('template/sessao', 'chamada') ?>
    </div>

    <div id="contato">
        <?php get_template_part('template/sessao', 'contato') ?>
    </div>

<?php
get_footer();