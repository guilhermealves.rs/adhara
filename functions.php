<?php
/**
 * Z Pixel functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package z_pixel
 */

require_once get_template_directory() . '/inc/general-functions.php';

/* Adicionar abaixo apenas funções específicas para tal projeto, funções globais devem ser armazenadas em '/inc/general-functions.php' */

/* ####################################################################################################################################*/