<div class="container">
    <div id="sessao-lojistas" class="link-menu"></div>
    <div class="coluna">
        <h2>Adhara na minha loja</h2>
        <h4>
            Adhara é uma marca feminina inspirada nas tendências da moda mundial, com preço
            competitivo e novidade constantes. <span>Nosso Maior desejo é ver seus clientes satisfeitos!</span>
        </h4>
        <h3>
            Ter adhara na sua loja traz vantagens lucrativas e seguras para o seu negócio.
            Consulte nossas políticas de compra, pagamento e entrega.
        </h3>
    </div>
</div>
<div class="chamada">Venha fazer parte da Adhara</div>