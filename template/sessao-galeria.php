<div class="container">
    <div id="sessao-galeria" class="link-menu"></div>
    <div class="heading">
        <h2>Coleção Outono Inverno</h2>
    </div>
    
    <div class="box-colecao">
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/IMG_6323.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/IMG_6323.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3330.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3330.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3340.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3340.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3383-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3383-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3490.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3490.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3500.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3500.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3575.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3575.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3585.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3585.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3659.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3659.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3703-Edit-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3703-Edit-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3750-Edit-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3750-Edit-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3756-Edit-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3756-Edit-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3767-Edit-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3767-Edit-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3778-Edit-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3778-Edit-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3869-Edit.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR3869-Edit.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR4032.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR4032.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR4039.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR4039.jpg');"></div></a></div>
        <div class="item-colecao"><a href="<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR4040.jpg"><div class="item" style="background: url('<?php echo get_template_directory_uri() ?>/assets/img/galeria/_STR4040.jpg');"></div></a></div>
    </div>

</div>