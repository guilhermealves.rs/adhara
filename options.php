<?php


/* Opções do tema */


function optionsframework_option_name() {

	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);
}


function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_check'),
		'two' => __('Two', 'options_check'),
		'three' => __('Three', 'options_check'),
		'four' => __('Four', 'options_check'),
		'five' => __('Five', 'options_check')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_check'),
		'two' => __('Pancake', 'options_check'),
		'three' => __('Omelette', 'options_check'),
		'four' => __('Crepe', 'options_check'),
		'five' => __('Waffle', 'options_check')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();
	// ------- Sessão Slide ---------- //
	// $options[] = array(
	// 	'name' => __('Quem Somos', 'options_check'),
	// 	'type' => 'heading');

	// $options[] = array(
	// 	'name' => __('Título da sessão', 'options_check'),
	// 	'desc' => __('Quem Somos.', 'options_check'),
	// 	'id' => 'quemsomos_titulo',
	// 	'std' => 'Título da sessão',
	// 	'type' => 'text');
	// $options[] = array(
	// 	'name' => __('Descrição', 'options_check'),
	// 	'desc' => 'Sobre a Sensolo',
	// 	'id' => 'box-2',
	// 	'type' => 'editor',);

	// $options[] = array(
	// 	'name' => __('Texto do botão', 'options_check'),
	// 	'desc' => __('Conheça a Sensolo.', 'options_check'),
	// 	'id' => 'text_btn_quemsomos',
	// 	'std' => 'Conheça a Sensolo',
	// 	'type' => 'text');
	// $options[] = array(
	// 	'name' => __('Link do botão', 'options_check'),
	// 	'desc' => __('Conheça a Sensolo.', 'options_check'),
	// 	'id' => 'link_btn_quemsomos',
	// 	'std' => 'Conheça a Sensolo',
	// 	'type' => 'text');
	// $options[] = array(
	// 	'name' => __('Email Consultoria', 'options_check'),
	// 	'desc' => __('Email.', 'options_check'),
	// 	'id' => 'email_quemsomos',
	// 	'std' => 'contato@sensolo.com.br',
	// 	'type' => 'text');
	// $options[] = array(
	// 	'name' => __('Imagem destacada', 'options_check'),
	// 	'desc' => __('Imagem da sessão Quem somos.', 'options_check'),
	// 	'id' => 'slide_2_dev',
	// 	'type' => 'upload');
	

	// // ------- Sessão Serviços ---------- //

	// $options[] = array(
	// 	'name' => __('Serviços', 'options_check'),
	// 	'type' => 'heading');

	// /* Box */

	// $options[] = array(
	// 	'name' => __('Titulo da sessão', 'options_check'),
	// 	'desc' => __('Serviços.', 'options_check'),
	// 	'id' => 'servicos_titulo',
	// 	'type' => 'text');

	// $options[] = array(
	// 	'name' => __('Os elementos dessa sessão você consegue editar em Painel > Serviços', 'options_check')
	// 	);


	// 	// ------- Sessão Projetos ---------- //

	// $options[] = array(
	// 	'name' => __('Projetos', 'options_check'),
	// 	'type' => 'heading');

	// $options[] = array(
	// 	'name' => __('Título da sessão', 'options_check'),
	// 	'desc' => __('Projetos.', 'options_check'),
	// 	'id' => 'projetos_titulo',
	// 	'std' => 'Título da sessão',
	// 	'type' => 'text');

	// $options[] = array(
	// 	'name' => __('Os elementos dessa sessão você consegue editar em Painel > Projetos', 'options_check')
	// 	);

	// // ------- Sessão Processo ---------- //

	// $options[] = array(
	// 	'name' => __('Processo', 'options_check'),
	// 	'type' => 'heading');


	// 	$options[] = array(
	// 	'name' => __('Título da sessão', 'options_check'),
	// 	'desc' => __('Processo.', 'options_check'),
	// 	'id' => 'processo_titulo',
	// 	'std' => 'Título da sessão',
	// 	'type' => 'text');

	// 	$options[] = array(
	// 	'name' => __('Descrição', 'options_check'),
	// 	'desc' => __('Descrição sobre o processo.', 'options_check'),
	// 	'id' => 'processo_desc',
	// 	'type' => 'textarea');

	// 	// ------- Sessão Depoimentos ---------- //

	// $options[] = array(
	// 	'name' => __('Depoimentos', 'options_check'),
	// 	'type' => 'heading');

	// $options[] = array(
	// 	'name' => __('Título da sessão', 'options_check'),
	// 	'desc' => __('Quem Somos.', 'options_check'),
	// 	'id' => 'quemsomos_titulo',
	// 	'std' => 'Título da sessão',
	// 	'type' => 'text');

	// $options[] = array(
	// 	'name' => __('Descrição Acessórios', 'options_check'),
	// 	'desc' => __('Descrição sobre Acessórios.', 'options_check'),
	// 	'id' => 'acessorios_descricao',
	// 	'std' => 'Descrição sobre Acessórios',
	// 	'type' => 'textarea');

	return $options;
}