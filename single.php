<?php

get_header(); ?>

    <div class="<?php global $post; echo $post->post_name;?> interna">

        <div class="conteudo-interno-blog">
            <div class="conteudo-post">
                <div class="<?php post_class() ?>">

                    <h1>
                        <?php the_title() ?>
                    </h1>

                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="entry-thumb">
                            <img class="img-destaque" src="<?php echo the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                        </div>
                        <?php the_content() ?>

                        <?php
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;
                        endwhile;
                        ?>
                </div>
            </div>
        </div>
        <?php get_template_part('template/sidebar', 'interna') ?>
        
    </div>
<?php
get_footer();