<?php wp_footer(); ?>

<!-- ############# Rodapé ############### -->
<footer id="rodape">
<div class="container">
  <div class="copyright">
    Todos os direitos reservados | Adhara 2018 ©
  </div>
  <div class="social">
    <a href="https://www.instagram.com/adhara.store/?hl=pt-br" target="_new"><i class="icon-instagram"></i></a>
    <a href="https://www.facebook.com/pages/Adhara-Store/240788439721984" target="_new"><i class="icon-facebook-square"></i></a>
  </div>
</div>
</footer>

<!-- ############# Scripts ############### -->

<!-- Scroll Function -->
<script>
    jQuery(function($){
        $(window).on('scroll', function() {
            scrollPosition = $(this).scrollTop();
            if (scrollPosition >= 50) {
                $("#cabecalho > header").css('top', '0px');
            }
            else if(scrollPosition <= 50 ) {
                $("#cabecalho > header").css('top', '30px');
            }
        });
    })
</script>

<script> 
jQuery(function($){
  $('.item-colecao a').simpleLightbox();
  $('#menu-adhara a').click(function(){
  $('#menu-adhara a').removeClass('ativo');
    $(this).addClass('ativo');
  })
})
</script>
<script>
  jQuery(function($){
    $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
    })
  })
</script>

<script> 
jQuery(function($){
  $('.menu-mobile').click(function(){
    $('#cabecalho nav').slideToggle();
  })
})</script>

<script> 
jQuery(function($){
  $('#lojista').attr('placeholder','Nome (lojista)');
  $('#loja').attr('placeholder','Nome da loja');
  $('#email').attr('placeholder','E-mail');
  $('#cnpj').attr('placeholder','CNPJ');
  $('#endereco').attr('placeholder','Endereço ( Loja Física )');
  $('#inscricao').attr('placeholder','Inscrição estadual');
  $('#mensagem').attr('placeholder','Escreva aqui suas observações, dúvidas e demais sugestões.');
})</script>

<!-- Preloader -->
<script>
// jQuery(function($){
// $(document).ready(function() {
//     $(window).on('load', function() {
//         $('#pageloader').css('opacity', '0').delay(350).hide(0);
//         $('body').css('overflow', 'auto');
//         });
//     });
// })
</script>

<!-- Feed Instagram -->

<script type="text/javascript">
// jQuery(function(){
//   var feed = new Instafeed({
//     get: 'user',
//     userId: '4159729019',
//     limit: '6',
//     accessToken: '4159729019.1677ed0.17d41b093f314df4a29e2c1e1923854e',
//     template: '<div class="item"><a target="_new" href="{{link}}"><img src="{{image}}" /></a></div>'
//   });
//   feed.run();
// })
</script>
</body>
</html>
