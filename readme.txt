=== ZPixel ===

Contributors: Zpixel
Tags: tema-próprio, site-institucional, zpixel

Requires at least: 4.0
Tested up to: 4.7
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

Um tema desenvolvido por Z Pixel!

== Description ==

Um tema desenvolvido exclusivamente para X, por favor, toda e qualquer alteração feita informar via email em: contato@zpixel.com.br

== Installation ==

1. No painel de administração, vá para Aparência > Temas e clique no botão Adicionar novo.
2. Clique em Carregar > Escolher Arquivo e selecione o arquivo .zip do tema. Clique em Instalar agora.
3. Clique em Ativar para usar seu novo tema imediatamente.

== Changelog ==

= 1.0 - 05 Abr 2018 =
* Release inicial

== Credits ==

* Baseado em: Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
