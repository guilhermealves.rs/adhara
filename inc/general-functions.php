<?php

show_admin_bar(false);

if ( ! function_exists( 'z_pixel_setup' ) ) :

function z_pixel_setup() {
	
	load_theme_textdomain( 'zpixel', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'zpixel' => esc_html__( 'Primary', 'zpixel' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	add_theme_support( 'custom-background', apply_filters( 'z_pixel_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'z_pixel_setup' );

function z_pixel_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'z_pixel_content_width', 640 );
}
add_action( 'after_setup_theme', 'z_pixel_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function z_pixel_scripts() {

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'z_pixel_scripts' );

require get_template_directory() . '/inc/custom-header.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/jetpack.php';

/*
 * Load site scripts.
 */
function scripts_do_tema() {
	$template_url = get_template_directory_uri();
	
	wp_enqueue_script( 'bootstrap-script', $template_url . '/js/bootstrap.min.js', array( 'jquery' ), null, true );
	// wp_enqueue_script( 'slick', $template_url . '/js/slick.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'zpixel', $template_url . '/js/zpixel.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'simplelightbox', $template_url . '/js/simple-lightbox.min.js', array( 'jquery' ), null, true );
	// wp_enqueue_script( 'instafeed', $template_url . '/js/instafeed.js', array( 'jquery' ), null, true );
	

    //Main Style
	wp_enqueue_style( 'font', $template_url . '/css/font.css' );
	wp_enqueue_style( 'bootstrap-style', $template_url . '/css/bootstrap.css' );
	wp_enqueue_style( 'zpixel', $template_url . '/css/zpixel.css' );
	wp_enqueue_style( 'simplelightbox', $template_url . '/css/simplelightbox.min.css' );
	// wp_enqueue_style( 'slick', $template_url . '/css/slick.min.css' );
	// wp_enqueue_style( 'slicktheme', $template_url . '/css/slick-theme.css' );
	
	}

add_action( 'wp_enqueue_scripts', 'scripts_do_tema', 1 );

/* Opções do tema */

if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = false) {
	
	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
		
	if ( isset($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}
$current_user = wp_get_current_user();
if ($current_user->user_login != '') {
    // admin styles
    function custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/assets/admin/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
    }
    add_action( 'admin_enqueue_scripts', 'custom_wp_admin_style' );
}

// login styles
function maisQueRoupa_login() {
    wp_enqueue_style('login', get_template_directory_uri() . '/assets/login/login.css', false, '1.0.0' ); 
}
add_action( 'login_enqueue_scripts', 'maisQueRoupa_login', 10 );


// Custom Excerpt

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }

    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

    return $excerpt;
}